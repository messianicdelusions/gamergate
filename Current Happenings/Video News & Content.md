## Video News & Content

<table>
  <tr>
    <td style="text-align: center; width:30%;"><h3>Vlogger</h3></td>
    <td style="text-align: center; width:30%;"><h3>Release Schedule</h3></td>
    <td style="text-align: center; width:30%;"><h3>Latest Release</h3></td>
  </tr>
  <tr>
    <td style="text-align: center;"><a href="https://www.youtube.com/channel/UCj6WU3IdNXHuwqv6WbcUH2Q" target="_blank"><img src="http://a.pomf.se/llbbnc.png"><br><b>Sockarina</b></a></td>
    <td style="text-align: center;">Daily</td>
    <td style="text-align: center;"><a href="https://www.youtube.com/watch?v=Ra4yD_wHpvQ&list=UUj6WU3IdNXHuwqv6WbcUH2Q" target="_blank">3-4 Oct 2014</a></td>
  </tr>
    <td style="text-align: center;"><a href="https://www.youtube.com/channel/UC1FantLFzgT7FDrWB_7eUKg" target="_blank"><img src="http://a.pomf.se/wuajyk.png"><br><b>GamerGate News</b></a></td>
    <td style="text-align: center;">Mon/Wed/Fri</td>
    <td style="text-align: center;"><a href="https://www.youtube.com/watch?v=kAxUAvojQ7g&list=UU1FantLFzgT7FDrWB_7eUKg" target="_blank">1 Oct 2014</a></td>
  </tr>
</table>