##### Please Fork and make a Pull Request if you want to see anything added!

## Current Happenings
After Gitlab staff [proved themselves](https://archive.today/nqNqr) to be [pretty based](http://a.pomf.se/ztjugk.jpg), we've decided to permanently migrate to Gitlab.

[#GamerGate Thunderclap started.](https://www.thunderclap.it/projects/17127-gamergate) It is [currently the #1 trending campaign.](http://a.pomf.se/detoir.png)

[Erik Kain announces a Livestream on GamerGate with Greg Tito, TotalBiscuit and Janelle Bonanno. Date: This monday, 2pm PST.](https://twitter.com/erikkain/status/518531324215570435)

[Appearently, InternetAristocrat has been doxxed.](https://twitter.com/Int_Aristocrat/status/518629853260558336)

Github disabled our repository. ~~As you can see, we've moved to Gitorious instead.~~  
[This is the email we've received from github.](http://a.pomf.se/cozqye.png)  
[Appearently a single rouge admin was responsible.](http://a.pomf.se/xdgspe.png)

[8chan hit by a DDOS attack.](https://twitter.com/infinitechan/status/518146964358459392) The site is back up now, [but the attacks are ongoing.](https://twitter.com/infinitechan/status/518202045229453313)

[Intel releases statement on why they've decided to pull their ads from Gamasutra.](http://33.media.tumblr.com/b88c9b60d4f43ec3bf5c98b03335f59d/tumblr_ncwarttyWQ1sm2yjco1_1280.png)

[Second episode of Radio Nero released.](https://soundcloud.com/radio_nero/series-1-episode-2-professional-failures)

[Intel pulls their ads from Gamasutra!](http://i.imgur.com/h5WqpM1.jpg) They admit, [they try to always do their best](https://twitter.com/IntelGaming/status/517494247939784704).

[Patreon CEO considers taking down The Sarkeesian Effect due to "offensive content".](http://i.gyazo.com/527e647085f0df7f8d5bb3a190ce0de5.png) Interestingly enough, it seems like [he also held a talk at XOXO.](https://pbs.twimg.com/media/By2mS1BCIAACj_C.jpg).  

[Cracked rejects pro #GamerGate article by Jamie Butterworth.](http://www.twitlonger.com/show/n_1sc93l4)

--

## Very slightly less Current Happenings

[Thunderf00t's suspension from Twitter is lifted](https://twitter.com/thunderf00t/status/516715454849900545)

[Total Biscuit](https://twitter.com/Totalbiscuit) tweets [a TwitLonger critiquing The Verge's attempts to inject gender politics into articles about video games](https://archive.today/I7nVl).

[APGNation interviews Willian Usher, the man behind the GameJournoPro leaks.](http://apgnation.com/archives/2014/09/29/7694/breaking-the-chain-an-interview-with-william-usher)

[Wikipedia admin shuts down GamerGate discussion.](http://i.imgur.com/FdkVvb4.jpg) Unfortunately, the biased and one-sided anti-GamerGate article will stay as it is for now. Just as a reminder: This is the first thing people see when they google "GamerGate". Something *needs* to be done about this.

KingOfPol claims to have been doxxed by a 4chan mod on /pol/. **Avoid posting on 4chan for now!**  
Tweets: [1](https://twitter.com/inawarminister/status/516180165332701186), [2](https://twitter.com/Kingofpol/status/516183675630014464), [3](https://twitter.com/Kingofpol/status/516183975132672000), [4](https://twitter.com/Kingofpol/status/516184214728114176),
[5](https://twitter.com/Kingofpol/status/516184410807623680)

[27th September - New Video from InternetAristocrat detailing a timeline of GamerGate since the begining of September] (https://www.youtube.com/watch?v=_dbi-8rPShE)

[Matt of TFYC thanks everyone involved for making their IndieGoGO campaign a success and announces that he will step down from the project.](http://thefineyoungcapitalists.tumblr.com/post/98525462265/on-endings)

[Christopher Arnold, developer of the indie game "Freak", explains why he is in support of GamerGate.](http://www.twitlonger.com/show/n_1sakrnh)

[Sargon of Akkad](https://twitter.com/Sargon_of_Akkad) releases his first video in a series on ["The Feminist Ideological Conquest of DiGRA"](https://www.youtube.com/watch?v=28D6_8KuIpc).

[VoD of latest InternetAristocrat stream.](https://www.youtube.com/watch?v=yS8bjnMlR5A)

[The Spectator](http://en.wikipedia.org/wiki/The_Spectator) releases [an article in favour of GamerGate](http://www.spectator.co.uk/columnists/james-delingpole/9322382/why-i-love-grand-theft-auto-v-and-the-feminazis-hate-it/).

[GamerGate related content is being deleted from archive.org.](https://twitter.com/RogueStarGamez/status/515022498203983872) Move to [archive.today](https://archive.today/) instead.

["We are Gamers. We are alive." as read by Big Man Tyrone.](https://www.youtube.com/watch?v=Q2v_Anr5cbs)

[8chan.co's full source code is now publicly available](https://twitter.com/infinitechan/status/514519596645486593) under [slight modifications of the MIT and X11 license](https://github.com/ctrlcctrlv/8chan/blob/master/LICENSE.md), [make a Github account](https://github.com/) and please [star and help contribute to the repository](https://github.com/ctrlcctrlv/8chan)!

[The #GamerGate and #notyourshield hashtags have reached over a million tweets combined!](https://pbs.twimg.com/media/ByPfEm0IEAEIKHc.png:large) Kudos to everyone involved.

[New Breitbart article on how Ben Kuchera tried to damage the image of Stardock's CEO](http://www.breitbart.com/Breitbart-London/2014/09/23/How-sloppy-biased-video-games-reporting-almost-destroyed-a-CEO)

[Titanium Dragon (a pro-GamerGate Wikipedia editor) has been banned from editing GamerGate and related articles.](http://i.imgur.com/gYvqWOA.jpg)

[Series 1, Episode 1 of Radio Nero](https://soundcloud.com/radio_nero/radioneroep1_1) comes out! Featuring [Adam Baldwin](https://soundcloud.com/radio_nero/radioneroep1_1), [Christina Hoff Sommers](https://twitter.com/CHSommers) and the [Internet Aristocrat](https://twitter.com/Int_Aristocrat)!

[KingofPol streams again](https://www.youtube.com/watch?v=OvwQoy1kYag), guests include [Sargon of Akkad](https://www.youtube.com/channel/UC-yewGHQbNFpDrGM0diZOLA), Roguestar, [Milo](https://twitter.com/Nero) of Breitbart News, Anti-#GamerGate member Kos, [Todd from Stralya (CounterTunes)](https://www.youtube.com/channel/UCZ908m6ZCkeyFPWSymp2riA) and [Synthovine](https://twitter.com/Synthovine) of KotakuinAction.  
[Starting at 02:32:56](https://www.youtube.com/watch?v=OvwQoy1kYag#t=2h32m56s), we hear from the Anti-#GamerGate side by Kos.

[KotakuInAction livestream](https://www.youtube.com/watch?v=SB8d5H_DB1w), features people from the Industry, [Hatman](https://twitter.com/thehat2) has probably a full list of who was in there.

[Pastebin from Little Mac (or Bob)](http://pastebin.com/VtzAxPXf) on how for instance he was heavily encouraged to contribute to a kickstarter of Anita Sakreesian or looses his job as well banned from Mighty No. 9 as well kickstarter rewards. He also gives some advice to the movement.  
Source: [Tweet from Hatman](https://twitter.com/TheHat2/status/514197489751429121) and [Livestream](https://www.youtube.com/watch?v=SB8d5H_DB1w).

Livestream by [@NotYourShield](https://twitter.com/NotYourShield): [The Women Of #GamerGate](https://www.youtube.com/watch?v=cjdiC2ednok).

[New MundaneMatt video](https://www.youtube.com/watch?v=dTxf6W3kdwU), Mundane Matt knows someone who is on the [Mailinglist](http://www.breitbart.com/Breitbart-London/2014/09/21/GameJournoPros-we-reveal-every-journalist-on-the-list).

[8chan.co is becoming a partner of 2ch (yes, *that* 2ch!).](https://twitter.com/infinitechan/status/514108117731516416)

New info regarding GameJournoPros published:  
["'They're On To Us': Gaming Journalists Respond to Critics in Newly Revealed GameJournoPros Emails"](http://www.breitbart.com/Breitbart-London/2014/09/22/They-re-on-to-us-gaming-journalists-respond-to-their-critics-in-series-of-new-GameJournoPros-emails)  
["The List: Every Journalist in the GameJournoPros Group Revealed"](http://www.breitbart.com/Breitbart-London/2014/09/21/GameJournoPros-we-reveal-every-journalist-on-the-list)  
[Summary of the emails on Milo Yiannopoulos' blog](http://yiannopoulos.net/2014/09/22/new-gamejourno-pro-leak-the-email-summaries/)

[Guest video by TheInvestigamer on GamerGate posted on TheAmazingAtheist's channel (600k subs)](https://www.youtube.com/watch?v=YCExXie1XB4)

[TotalBiscuit talks about recent events.](https://www.youtube.com/watch?v=2fYSOPifbUk) Seems like he's on the verge of losing his neutral stance...

[John Carmack BTFOs a SJW during the Oculus Connect Keynote.](https://www.youtube.com/watch?v=vzmbW4ueGdg)

[Someone made an offer to to buy 8chan.](http://i.imgur.com/eKbYi7v.jpg)

[Another Anti-GamerGate piece by Washington Post.](https://archive.today/MhNiP)

[TFYC voting has ended; Afterlife Empire is the winner.](http://thefineyoungcapitalists.tumblr.com/post/98028000740/afterlife-empire-recieve-the-most-votes)

[All GamerGate discussion has been banned from the TvTropes forums](https://pbs.twimg.com/media/ByCibyfCAAACyuQ.png:large), [tweet by IA](https://twitter.com/Int_Aristocrat/status/513588957473292290).

[Someone was banned from viewing the Mighty No. 9 twitter account for supporting GamerGate.](https://pbs.twimg.com/media/ByBZB2eCYAEkXKR.png)

[Polar Roller@j_millerworks](https://twitter.com/j_millerworks), [creator of #NotYourShield](https://twitter.com/IonCaron/status/508542963832860672), is [fired from his job due to complaints from SJWs](https://twitter.com/j_millerworks/status/513198848542781440).

[Sargon of Akkad explains why GamerGate is so important to him.](https://www.youtube.com/watch?v=yZI2IRbSKts) His explanation will probably resonate with most of us.

[The Escapist is suffering from a DDoS attack. Specifically, the attackers are targeting the GamerGate thread.](https://twitter.com/archon/status/513365103329423360)

[Milo Yiannopoulos](https://twitter.com/Nero) has a public fall out with [Liana Kerzner](https://twitter.com/redlianak) (Polygon Contributor) over [possible screenshots of Ben Kuchera's mails to GameJournoPros](https://twitter.com/Nero/status/513016521753628672).  
Conversation: [1](https://twitter.com/Nero/status/513053224400859136), [2](https://twitter.com/redlianak/status/513057496064589825), [3](https://twitter.com/Nero/status/513059027161735168), [4](https://twitter.com/Nero/status/513059688024645633), [5](https://twitter.com/Nero/status/513067602709864449)

[Someone ("a group") is trying to hack into various accounts of Matt (TFYC) again.](https://twitter.com/TFYCapitalists/status/513048546858532864)

[PressFartToContinue was banned from twitter.](https://twitter.com/fartchives/status/513041035862503424)

[Out of all possible sites, freaking FunnyJunk makes an announcement saying that GamerGate discussion is allowed.](http://www.funnyjunk.com/gamergate+sjw+discussion+allowed/text/5299181/) *Just to make this more clear: Discussion not allowed on 4chan. Discussion allowed on Epicmaymay-Paradise. I'm at a loss of words again.*

Milo Yiannopoulos exposes "GameJournoPros", a secret mailing list on which several prominent gaming "journalists" "discuss what to cover, what to ignore, and what approach their coverage should take to breaking news".  
[His article with all the important information.](http://www.breitbart.com/Breitbart-London/2014/09/17/Exposed-the-secret-mailing-list-of-the-gaming-journalism-elite)  
[Milo confirms that more details are still unpublished.](https://twitter.com/Nero/status/512353477628940290)  
[All of the GameJournoPros were dumped. Censored version with no personal info here.](http://yiannopoulos.net/2014/09/19/gamejournopros-zoe-quinn-email-dump/)

[A Conversation with Polygon Contributor Liana Kerzner about #GamerGate (Sargon of Akkad and Liana Kerzner)](https://www.youtube.com/watch?v=QuHfu3W1BGE)

Mod of [subreddit KotakuInAction](http://www.reddit.com/r/KotakuInAction/) ([Hatman](https://twitter.com/thehat2)), [hosts a LiveStream](https://www.youtube.com/watch?v=0FlVaHXqaQg) featuring [MundaneMatt](https://twitter.com/MundaneMatt), [TotalBiscuit](https://twitter.com/Totalbiscuit), [Reddit/r/gaming Mod](http://www.reddit.com/user/synbios16), [Lo-Ping](https://twitter.com/GamingAndPandas), [Devi Ever](https://twitter.com/deviever), [NotYourShield](https://twitter.com/NotYourShield), [Andrew Otton (techraptor writer)](https://twitter.com/OttAndrew) and many more (check the [video description!](https://www.youtube.com/watch?v=0FlVaHXqaQg)).

[KingfPol](https://twitter.com/Kingofpol) hosts a [Stream of Happenings at hitbox](http://www.hitbox.tv/kingofpol), featuring a Goodgamers.us writer, [Matt (TFYC Founder)](https://twitter.com/TFYCapitalists), [Milo Yiannopoulos](https://twitter.com/Nero), [Adam Baldwin](https://twitter.com/AdamBaldwin) and many more.  
Recording of the stream has been [uploaded to youtube here](https://www.youtube.com/watch?v=1B1cvL1Fn4U).

[Livechan developer adds a /v/ board](http://a.pomf.se/nbmkky.png) for [Video Game discussion](https://livechan.net/chat/v) and [GamerGate + NotYourShield](https://livechan.net/chat/v#%23GG%2B%23NYS).

[Moot speaks up, confirming that GamerGate discussion is no longer allowed on /v/.](http://boards.4chan.org/v/thread/264227139/regarding-recent-events) ~~*moot removed the sticky, luckily there's still an archived copy of it*~~ *Update: Now the sticky is back. It's a new thread with the same text as before. No idea what that is all about.*   
[Migrate](http://a.pomf.se/khegwn.png) to [/burgers/](https://8chan.co/burgers/) or [/v/](https://8chan.co/v/) on 8chan instead.  
As expected, most people are not all too happy with that announcement. [/v/ is being flooded in protest,](http://i.imgur.com/WorR3KO.jpg) [as are /sp/](https://i.imgur.com/iRLPyap.png) and some other boards.  

[Summary of InternetAristicrat's stream. Read this if you don't know what to do next.](http://pastebin.com/KGwY37Fn)

[Richard Dawkins posts a tweet in support of C.H. Sommers.](https://twitter.com/RichardDawkins/status/512110699435524096)

GamerGate-related threads are being deleted and OPs banned from [/v/](https://pbs.twimg.com/media/Bxriv52CMAIeG-r.jpg) once again. The OP here on github is being autofiltered as well.  
[People are being banned/kicked from IRC.](http://i.imgur.com/dtzjFBr.png)  
[Appearently threads are being deleted because of "spamming".](http://i.imgur.com/n74rwJ4.png)  
[4chan's feedback page just 404'd.](http://www.4chan.org/feedback)  
[They aren't even bothering anymore with giving a proper reason for banning.](https://twitter.com/DignitasSunBro/status/511986066132598784)  
[More IRC logs.](https://pbs.twimg.com/media/BxrjxO2IQAAIsok.png:large)  
[Mods banned a bunch of German IP ranges, affecting people who have never even heard of GamerGate](https://www.youtube.com/watch?v=OcV2RCvPKvM).

[Polygon is at it again, this time attempting to discredit C.H. Sommers as a "conservative" among other mind-bending things.](https://archive.today/BQbD7)

["Are video games sexist?" by Christina H. Sommers. Trigger Warning: BTFO's SJWs to hilarious degrees.](https://www.youtube.com/watch?v=9MxqSwzFy5w)

[Some thoughts by InternetAristocrat on how to proceed with GamerGate. Also, he announced a livestream. 9/17/2014 at 6pm central standard time.](http://pastebin.com/tAppaZjy)

[Moot atended XOXO, the place where Sarkeesian hold her "listen and beLIEve"-speech.](https://pbs.twimg.com/media/Bxsc_qeCMAAnvlO.png#_=_)

[Thunderf00t was suspended from twitter.](https://www.youtube.com/watch?v=6a4vaZy0a18)

["I believe that Kotaku writers are indeed entitled to pay into a game developer Patreon if that's what they need to do to access a developer's work for coverage purposes. They can even expense it." -Stephen Totilo](https://archive.today/ofdPK)  
Sometimes, I really wish I was making some of these happenings up.

[Infographic for people coming to GamerGate from the WikiLeaks tweets](http://a.pomf.se/zbgeak.png)

[Julian Assange on reddit censorship](http://a.pomf.se/qwmnxm.png)

[GamerGate gets a supportive shout-out from WikiLeaks!](https://twitter.com/wikileaks/status/511727048931282944)

[The wikipedia article on GamerGate is completely unbiased! (not)](http://i.imgur.com/pHt972J.jpg)

**NOTE:** *Try to give this person the least amount of attention possible. Don't make any tweets about/directed at her, don't watch her videos, don't spread non-archived article about her, etc.*   
Sarkeesian holds a speech at XOXO Festival and calls for a "Feminist Army". [Includes discrediting GamerGate as a conspiracy theory via cherry-picked tweets](http://i.imgur.com/ju4wdDc.jpg) all that stuff you would expect.  
[I can't make this shit up:](https://pbs.twimg.com/media/BxgTiF_IcAAYEEl.jpg) ["Listen and believe"](http://i.imgur.com/eVZTv7m.jpg)  
["A room full of mid-30s white people call discuss "oppression" and "diversity" in gaming"](https://pbs.twimg.com/media/BxftFbRIMAI_LGu.jpg:large)  
[BuzzFeed in full support of this](https://archive.today/oG0P9)  
[As is The Verge](https://archive.today/5UQjh)  

[techraptor interviews Daniel Vavra on GamerGate](http://techraptor.net/2014/09/12/interview-daniel-vavra/)

[goodgamer.us interviews TFYC](http://www.goodgamers.us/2014/09/14/interview-the-fine-young-capitalists/)

[Yet another publication trying to discredit GamerGate, this time Cracked](https://archive.today/0gGBF)

[LeoPirate Video on 9/11 Harassment](http://youtu.be/vOoJuRYIRjM)

[boogie2988 on the backslash he has received for supporting GamerGate](https://pbs.twimg.com/media/Bxf4yFfIQAELerF.png:large)

[Interview with Greg Lisby, regarding ethics in journalism.](https://www.youtube.com/watch?v=4-7RLxrsJ04)

[Ben Kuchera of Polygon refuses to speak with a transgender female game dev after hearing she is in support of GamerGate](https://twitter.com/deviever/status/510547381297758208)  

[Gamasutra's Alexa rank continues to drop.](http://i.imgur.com/JScbNnU.png)  

http://www.staresattheworld.com/2014/09/anita-sarkeesian-fabricate-story-contacting-authorities/  

[goodgamers.us launched: "All of the gaming news, none of the bullshit"](http://www.goodgamers.us/)  

[Milo Yiannopoulos](http://en.wikipedia.org/wiki/Milo_Yiannopoulos)[@Nero](https://twitter.com/Nero) makes a [thread on 4chan](https://twitter.com/Nero/status/510221194164187136) that reaches [2660 replies and 795 images](http://a.pomf.se/vsybnt.png).   [Jadusable](https://www.youtube.com/user/Jadusable)[@AlexanderDHall](https://twitter.com/AlexanderDHall) also [posts in the thread](https://twitter.com/AlexanderDHall/status/510232373364285441).  
Archived thread: https://archive.moe/v/thread/262952626/

[UniLever confirms they will no longer advertise with Polygon](https://twitter.com/SHG_Nackt/status/510012273830932480)

[Developer speaks out, "...GamerGate's been a long time coming."](https://archive.today/ix6Wv)

**TFYC SUCCESSFULLY FUNDED ON 09/11! GOOD JOB GUYS!**  
[Statement by TFYC](http://thefineyoungcapitalists.tumblr.com/post/97242984635/on-broken-phones-and-meeting-goals)  
[spark82 CONFIRMED AS THE HERO OF VIDYA](http://i.imgur.com/c1O6pFf.jpg)  

[GamerGate is now spreading to Japan as well!](http://a.pomf.se/dcgvrw.png)

[Very detailed pastebin showing how the Gamer+ clique (Silverstring, DiGRA, Gamasutra, among others) is connected to one another.](http://pastebin.com/PmdSbPHN)

[Joe Rogan (1.41M followers) is interested in covering GamerGate.](http://i.imgur.com/PqZyj4v.png)

[Jason Schreier's "justification" as to why he is not writing about TFYC.](http://www.twitlonger.com/show/n_1s9dufq)

[Speaking of NeoGAF; TFYC were banned from said site.](https://pbs.twimg.com/media/BxQDNcNCEAEAQqu.jpg:large)

[Yet another anti-GamerGate article by The Telegraph](https://archive.today/e6Jub)

[Another article trying to discredit GamerGate surfaced, this time voicing support for NeoGAF out of all sites.](https://archive.today/xbPE1)

[Polygon loses Scottrade as an an advertiser!](https://twitter.com/Chriss_m/status/509486352174694400)

[Kotaku gone from the list of StackSocial affiliates!](https://stacksocial.com/business/affiliates)

[APGNation's interview with TFYC, giving more details on how Zoe hindered them](http://apgnation.com/archives/2014/09/09/6977/truth-gaming-interview-fine-young-capitalists)

A bunch of articles trying to discredit #GamerGate as a misogynistic movement created by conspiring 4chan users were published in the past ~24 hours:  
[1. arstechnica: Chat logs show how 4chan users created #GamerGate controversy](https://archive.today/GC5MS)  
[2. Wired (Note: same author as the arstechnica article): How 4chan manufactured the #GamerGate controversy](https://archive.today/LTFr9)  
[3. The New Yorker: Zoe Quinn's Depression Quest](https://archive.today/nb14y)  
[4. Bustle: What Is "#Gamer Gate"? It's Misogyny, Under The Banner Of "Journalistic Integrity"](https://archive.today/UWsE3)  
[5. The Telegraph: Misogyny, death threats and a mob of trolls: Inside the dark world of video games with Zoe Quinn - target of #GamerGate](https://archive.today/vdsyJ)  

["#GamerGate is winning!!!": A pretty motivating video by MundaneMatt detailing how we actually *are* having an effect on gaming media.](https://www.youtube.com/watch?v=nENGp6d684I)

[TotalBiscuit rambles about the gaming media and current events for 30 minutes](https://www.youtube.com/watch?v=e78JRIHRjC0)

Two supporters of #GamerGate doxxed; Zoe proceeds to tweet the dox around  
[1. Censored screencap of the dox](http://i.imgur.com/He7UCVW.png)  
[2. Screencap of Zoe's tweet](http://i.imgur.com/k96uLWF.jpg)  
[3. Zoe attempting to backpedal from the spreading of the dox](https://archive.today/lIn6s)

IGF, INDIECADE AND POLYTRON BTFO'D ON STREAM BY LORDKAT - Hard evidence for racketeering leaked:  
[1. Transcript of the stream](http://www.lordkat.com/igf-and-indiecade-racketeering.html)  
[2. Infograph](https://pbs.twimg.com/media/Bw-dYT_CIAActWR.jpg:large)  
[3. Video by ShortFatOtaku (Has been made Private)](https://www.youtube.com/watch?v=HM_Z5YTop7g)  

Evidence of leddit admins/mods "censoring, reading personal messages, shadowbanning, altering content, and threatening people with charges of CP" surfaced:  
[1. Soundcloud link] (https://soundcloud.com/user613982511/recording-xm-2014)  
[2. Indiejuice article](http://indiejuice.tv/moderating-front-page-internet/)  
[3. One admin denying claims of censorship](http://a.pomf.se/ycliin.png)  
[4. Reddit in full damage control](http://a.pomf.se/chzjot.png)  
[5. /r/gaming mod trying to justify the deletion of a submission of TotalBiscuit's new video](http://puu.sh/bsC0f/132c0012be.png)  

Zoe lurks on 4chan IRC channels, makes logs, then post these logs on her twitter out of content under the label "GamerOverGate":  
[1. Video by MundaneMatt on this topic](https://www.youtube.com/watch?v=uv0Fg20i1iA)  
[2. Rebuttal of the IRC logs](http://i.imgur.com/cBv8Wx5.png)  
[3. Response to #GameOverGate](http://i.imgur.com/g385IJM.jpg)

Der Spiegel, a once quite respected German news magazine, publishes an article heavily biased against GamerGate:  
[1. Scan of the article](http://i.imgur.com/QvFixou.jpg)  
[2. Translation by someone on leddit](http://reddit.com/r/KotakuInAction/comments/2fsxf3/der_spiegel_a_german_newspaper_printed_an_article/)  

[Some dipshit spams Anita Sarkeesian's twitter with CP](https://www.youtube.com/watch?v=hsDpX9WSXnc)

[VoD of InternetAristocrat's "shitaku" stream, in which he talks about GamerGate related stuff](https://www.youtube.com/watch?v=TpwyEq_m0zc)

[#GamerGate and related stuff still increasing in activity on twitter](http://i.imgur.com/uzm5MUc.png)

[Los Angeles Times joins the Gamer+ side](http://imgur.com/a/cDYtY)

[Matt of TFYC on Honey Badger Radio](http://honeybadgerbrigade.com/radio/honey-badger-radio-quinnspiracy-2-electric-boogaloo/)

[InternetAristocrat and Adam Baldwin on The Ed Morrissey Show](http://www.ustream.tv/recorded/52292531)

-

## Slightly Less Current Happenings

[LeoPirate on #GamerGate and corruption](http://youtu.be/9rTFDhVmnUE)

[Boogie releases a new video supportin GamerGate!](https://www.youtube.com/watch?v=wbQk5YqjO0E)

Adam Baldwin and Internet Aristocrat arrange meeting at Radio Show by [EdMorrisey on ustream](http://www.ustream.tv/channel/the-ed-morrissey-show): http://a.pomf.se/nrawuw.png

[The Guardian](http://en.wikipedia.org/wiki/The_Guardian) cuts off freelance writer Jenn after [her writing of an article biased against GamerGate](http://a.pomf.se/qnkglm.png): read [tweet 1](https://twitter.com/jennatar/status/507411245717135360), [tweet 2](https://twitter.com/jennatar/status/507411245717135360)

[Lawfag cancels his indiegogo campaign](https://www.indiegogo.com/projects/lawyers-against-gaming-corruption#activity) after hosting an [Ask Me Anything on Reddit](http://www.reddit.com/r/AMA/comments/2feuyz/attorney_representing_goodgamersus_ama/).  He is working on refunding all donations.

[NOTE: now cancelled] Lawfag launches his indiegogo campaign, "Lawyers Against Gaming Corruption": http://igg.me/at/GamerGate/x/8571466

Gaming site promising to have actual journalistic standards is announced: http://www.goodgamers.us/

Mod on #4chan IRC kicking people for complaining about the banning and autosaging: http://i.imgur.com/pBzNGlV.png

[NOTE2: autosaging is back agan] [NOTE: not anymore it seems] /v/ was autosaging #gamergate threads and banning OPs: http://i.imgur.com/yGNIMpI.png

Based female gamer giving her opinion on this whole subject: https://www.youtube.com/watch?v=tyscI9wZ8Bk

New MundaneMatt video dissecting the assertion gamers are "like nazis": https://www.youtube.com/watch?v=TO589ti_-Pc

Ben Quintero "downgraded" on gamasutra: http://loltaku.tumblr.com/post/96517102414/here-is-the-article-he-was-demoted-for-writing

Lawfag and IA might have some glorious plans for the future: http://a.pomf.se/ohuszo.png

[NOTE: he's back now with a new trip] lawfag banned from /v/: http://ask.fm/Lawfag/answer/119126950824

Based boogie2988 starts a petition on change.org against the agenda versus gamers: http://www.change.org/p/the-gaming-industry-please-stop-the-hate

Pastebin of a deleted google doc detailing how Silverstring Media is trying to push a "feminist" agenda in the gaming industry - http://pastebin.com/hnxggVBw

Daniel Vavra, designer of the Mafia series, is on our side: http://a.pomf.se/rxtbws.png

Gamasutra, Polygon, Kotaku and RPS loosing traffic (possibly due to #GamerGate?): http://i.imgur.com/1wyEap9.png

The BBC publishes a one-sided and biased pro-Sarkeesian article: https://archive.today/RpO7w

The Guardian edits their article with a disclaimer stating the author "purchased and supported [Sarkeesian's] work": http://i.imgur.com/tsCj6fw.png

@legobutts selling t-shirts based on this drama (she's directly involved in) and might be profiting of it: http://i.imgur.com/lDdFWhI.png

Totalbiscuit's opinion on pretentious indie devs (warning: singing): https://soundcloud.com/totalbiscuit/shania-bain-that-dont-impressa-me-much

How to win IndieArcade: http://i.imgur.com/x3HO47j.jpg

SR4 creative director voices his support for Anita Sarkeesian: https://archive.today/cebK5

IGN joined the Gamer+ side (no surprise here): https://archive.today/RsfWv

-

## Older Happenings:

breitbart.com article, pretty heavily biased but still in support of us - http://www.breitbart.com/Breitbart-London/2014/09/01/Lying-Greedy-Promiscuous-Feminist-Bullies-are-Tearing-the-Video-Game-Industry-Apart

op disrespectul nod in full effect, keep it up - http://i.imgur.com/sdL2EAN.jpg

they have shills at the fucking guardian now - https://archive.today/eHC9z

vox media possibly coming under pressure from samsung due to this mess? - http://i.imgur.com/zNpjpxp.jpg

people being escorted off PAX for mentioning #gamergate? - https://twitter.com/joerogan/status/320004871346606080 http://i.imgur.com/p42lPz3.png

boogie2988 is on /v/ and supports us - http://a.pomf.se/nedoaq.png

The Escapist takes a stand for integrity - http://www.escapistmagazine.com/forums/read/18.858347-Zoe-Quinn-and-the-surrounding-controversy?page=375#21330098

Evidence for possible collusion? - http://markdownshare.com/view/a524affd-e679-40be-8aa1-72058065dc2a http://i.imgur.com/ZJ0nDpt.jpg

Maya Felix Kramer (aka @legobutts) Call Out - http://pastebin.com/qWX5qTvB

Leigh Alexander calling Adam Baldwin a crackhead - http://i.imgur.com/Dw4oAnq.png

Leigh Alexander getting BTFO'd by MundaneMatt - https://www.youtube.com/watch?v=XJf6sFThGbs

Christina H. Sommers (an actual feminist) supports our cause - http://i.imgur.com/pavWden.png

Possible happening on monday at PAX - http://i.imgur.com/0TMWn75.png http://i.imgur.com/Bq8Dywe.jpg

New IA video incoming - https://twitter.com/Int_Aristocrat/status/505667357990060032

Techraptor shadowbanned from reddit for reporting on this - http://i.imgur.com/I9ssnGJ.png

this "gamer+" shit has been in planning for over a year now - https://groups.google.com/forum/#!topic/game-words-incorporated/VxYXZhDqv8I

Cliffy B has a brother who works for the conglomerate Polygon belongs to - http://i.imgur.com/DAbE8nY.jpg

FIRE RISES - Losing ROPE
http://i.imgur.com/qMMVHER.jpg

ISIS joke by BETA SJW
http://i.imgur.com/sNCIuBR.png

a larian studios (divinity: original sin) employee speaks out against SJWs affecting gaming: http://orogion.deviantart.com/journal/Save-the-Boob-plate-380891149

Yellow journalism Mk 2 - https://archive.today/3QoY3

Based TFYC - https://www.youtube.com/watch?v=rn7Q4XbWqvQ&list=UUhwoDCOjliin3x0Y_ShiGGw

Story of TFYC - http://youtu.be/1d6Q3VpqXyk

Karen Straughan talks - https://www.youtube.com/watch?v=cuFSMi5G3R4

What Really happened(MundaneMatt Guest) - https://www.youtube.com/watch?v=FV_8RHSyS14

Zoe Quinn on TFYC - http://youtu.be/xiWADnV8pmw

IndieGoGo campaign - https://www.indiegogo.com/projects/the-fine-young-capitalists--2/x/8485163

Based Jontron rant - http://youtu.be/cf7i0z2Zi7U?t=1h17m2s

Screenshot hacked IndieGoGo - http://i.imgur.com/EKNGSDJ.jpg
(btw no fiddles and fries, if the project doesn't get funded we get our money back)

Censorship on Reddit - http://imgur.com/a/f4WDf

More Conflict of Interest/Nepotism - http://imgur.com/a/bqhRY
http://imgur.com/a/x0NpT
http://i.imgur.com/Wg6iLvL.jpg

Wolf Wozniak PAX Passes - http://i.imgur.com/cr6tfHr.jpg
Ex-boyfriend speaks - http://www.reddit.com/user/qrios
Ex-boyfriend speaks part deux -http://antinegationism.tumblr.com/post/95602910146/what-all-that-zoe-quinn-stuff-was-about-a-totally

Banning on Steam Forum for Critique
http://i.imgur.com/VwHDs2Z.jpg

Kotaku breaking their own rule on ETHICS
http://i.imgur.com/hE1US8k.png
